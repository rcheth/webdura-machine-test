import * as mongoose from "mongoose";
import { sanitizeSave, sanitizeSchema } from "mongo-sanitize-save";
export interface IBooking {
  mobile: string;
  booking_status: string;
  invoice_generated?: boolean;

}
let Schema = mongoose.Schema;
let mySchema = new Schema(
  {
    mobile: { type: String, required: true },
    booking_status: { type: String, default: "pending" },
    invoice_generated: { type: Boolean }
  },
  {
    timestamps: true,
  },
);

sanitizeSchema(mySchema, { skip: [""] });

export interface IBookingModel extends IBooking, mongoose.Document { }

export default mongoose.model<IBookingModel>("bookings", mySchema);
