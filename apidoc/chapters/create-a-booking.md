# Create a booking
> This POST method API creates a booking request made by user.
------
## Headers
| KEY | Value |
|-----|------|
| URL | **`/v1/bookings`** |
| METHOD | **POST** |
| content-type | **application/json** |
------
## Request
```javascript
{
    "mobile": "9425012823"
}

```
------
## Success
```javascript
{
    "booking_status": "PENDING",
    "_id": "6189114f1994763521f346cf",
    "mobile": "9425012823",
    "createdAt": "2021-11-08T12:00:15.101Z",
    "updatedAt": "2021-11-08T12:00:15.101Z",
    "__v": 0
}

```
------
## Error
```javascript
{
    "error": "Error: Path `mobile` is required."
}
```