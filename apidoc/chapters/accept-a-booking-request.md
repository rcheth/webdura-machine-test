# Accept a booking request
> This PUT method API accepts a booking request made by user.
------
## Headers
| KEY | Value |
|-----|------|
| URL | **`/v1/bookings/:bookingId/active`** |
| METHOD | **PUT** |
| content-type | **application/json** |
------
## Request
```javascript
{}

```
------
## Success
```javascript
{
    "success": true,
    "modified": true
}

```
------
## Error
```javascript
{
    "error": "CastError: Cast to ObjectId failed for value \"618911471994763521f346a\" at path \"_id\" for model \"bookings\""
}
```