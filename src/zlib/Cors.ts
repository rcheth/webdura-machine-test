import * as express from "express";
import AppConfigUtil from "../zlib/AppConfigUtil";
const cors = require("cors");

export default class Cors {
  static initializeCors(app: express.Application) {
    let domains: string[] = AppConfigUtil.get(`cors_domains`);
    var whitelist: RegExp[] = domains.map((domain) => {
      let reg_str = domain.split(".").join(".") + "$";
      let regex = new RegExp(reg_str);
      return regex;
    });

    let corsOptionsWhitelist = { origin: whitelist, optionsSuccessStatus: 200 };

    app.options("*", cors(corsOptionsWhitelist));
    app.use(cors(corsOptionsWhitelist));
  }
}
