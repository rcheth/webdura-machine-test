import appConfig from "../config/appConfig";
export default class AppConfigUtil {
  static get(path: string): any {
    return appConfig.get(path);
  }

  static set(path: string, value: any): any {
    return appConfig.set(path, value);
  }
}
