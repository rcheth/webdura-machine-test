import * as express from "express";
import BookingHelper from "../zlib/BookingHelper";
export default class BookingController {
    static createBooking(req: express.Request, res: express.Response, next: express.NextFunction) {
        BookingHelper.newBooking(req.body).then(
            (result) => {
                res.send(result);
            },
            (err) => {
                res.status(400).send({ error: err.toString() });
            },
        );
    }
    static fetchBookings(req: express.Request, res: express.Response, next: express.NextFunction) {
        BookingHelper.fetchBookings(req.params).then(
            (result) => {
                res.send(result);
            },
            (err) => {
                res.status(500).send({ error: err.toString() });
            },
        );
    }
    static fetchBookingsByStatus(req: express.Request, res: express.Response, next: express.NextFunction) {
        BookingHelper.fetchBookingsByStatus(req.params).then(
            (result) => {
                res.send(result);
            },
            (err) => {
                res.status(500).send({ error: err.toString() });
            },
        );
    }
    static acceptBooking(req: express.Request, res: express.Response, next: express.NextFunction) {
        BookingHelper.acceptBooking(req.params.id).then(
            (result) => {
                res.send(result);
            },
            (err) => {
                res.status(500).send({ error: err.toString() });
            },
        );
    }
    static generateInvoice(req: express.Request, res: express.Response, next: express.NextFunction) {
        BookingHelper.generateInvoice(req.params.id).then(
            (result) => {
                res.send(result);
            },
            (err) => {
                res.status(500).send({ error: err.toString() });
            },
        );
    }
}
